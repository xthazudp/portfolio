import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import About from './About/AboutComponent';
import Footer from './Common/Footer/FooterComponent';
import Header from './Common/Header/HeaderComponent';
import Contact from './Contact/ContactComponent';
import Home from './Home/HomeComponent';
import Services from './Services/ServicesComponent';

import 'bootstrap/dist/css/bootstrap.min.css';

// import './AppComponent.css';
// import './AppComponent.scss';

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route path="/services" component={Services} />
          <Route path="/contact" component={Contact} />
          <Route path="/contact" component={Contact} />
        </Switch>
        <Footer />
      </BrowserRouter>
    </div>
  );
};

export default App;
