import React from 'react';
import { Link } from 'react-router-dom';
import InfoBox from '../Common/InfoBox/InfoboxComponent';
import Testimonial from '../Common/Testimonial/TestimonialComponent';
import WelcomeArea from '../Common/WelcomeArea/WelcomeAreaComponent';

const About = () => {
  return (
    <div>
      {/* <!--================Home Banner Area =================--> */}
      <section class="banner_area">
        <div class="box_1620">
          <div class="banner_inner d-flex align-items-center">
            <div class="container">
              <div class="banner_content text-center">
                <h2>About Us</h2>
                <div class="page_link">
                  <Link to="/">Home</Link>
                  <Link to="/about">About Us</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!--================End Home Banner Area =================--> */}
      {/* <!--================Home Banner Area =================--> */}
      <InfoBox />
      {/* // <!--================End Home Banner Area =================--> */}

      {/* // <!--================Welcome Area =================--> */}
      <WelcomeArea />
      {/* // <!--================End Welcome Area =================--> */}
      <Testimonial />
    </div>
  );
};

export default About;
